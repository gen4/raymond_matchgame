var express 			= require('express');
var app 				= express();
var fs					= require('fs');
var multer 				= require('multer');
var db 					= require('./server/db.js');

var pubnub				= require('./pubnub.js');

var uploadDir = __dirname +'/server/uploads/';
var upload = multer({ dest: uploadDir });

app.use('/', express.static(__dirname + '/app'));
db.init(function(err){
	if(err){

	}else{
		startServer();
	}
});

app.use('/images', express.static(uploadDir));

app.post('/upload', upload.single('file'), function(req,res){
	console.log('upload ',req.file);
	if(req.file && req.file.filename){
		db.settings().images.push({filename:req.file.filename,available:true});
		db.save(function(err){
			res.json({result:'ok'});
		})
	}else{
		res.json({error:'unknown'});
	}
});

app.post('/sponsor', upload.single('file'), function(req,res){
	if(req.file && req.file.filename){
		db.settings().sponsor= {filename:req.file.filename};
		db.save(function(err){
			res.json({result:'ok'});
		})
	}else{
		res.json({error:'unknown'});
	}
});

app.delete('/sponsor', function(req,res){
	db.settings().sponsor= {};
	db.save(function(err){
		res.json({result:'ok'});
	})
});


app.delete('/file/:id',function(req,res){
	fs.unlink(uploadDir+req.params.id,function(err,res){
		console.log('deleted');
	});
	res.json({result:'ok'});
})

app.get('/admin',function(req,res){
	res.sendFile(__dirname + '/app/admin.html');
});


app.get('/defaults',function(req,res){
	res.json(db.settings());
});

function startServer(){
	var server = app.listen(process.env.PORT || 3000, function () {
		var host = server.address().address;
		var port = server.address().port;

		console.log('Example app listening at http://%s:%s', host, port);
	});
}
