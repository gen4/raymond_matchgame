var pubnub = require("pubnub")({
	ssl           : true,  // <- enable TLS Tunneling over TCP
	publish_key   : "pub-c-0bb265cb-f829-414d-923b-a838e0a9cc04",
	subscribe_key : "sub-c-10c5ffd0-f18f-11e5-8180-0619f8945a4f"
});
var db 					= require('./server/db.js');
var _					= require('lodash');

pubnub.subscribe({
	channel  : "MemoryGame:March:gen4",
	callback : function(message) {
		message = message.message;
		switch(message.cmd){
			case 'newSettings':
				db.change(message.data);
				db.save();
				break;

			case 'changeAvailable':
				console.log('changeAvailable',message.data);
				_.findLast(db.settings().images,{filename:message.data.filename}).available = message.data.isAvailable;
				db.save();
				break;
			default:
				break;
		}
	}
});

module.exports = {

}