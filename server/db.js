var fs = require("fs");
var async = require('async');
var _		= require('lodash');
var settings;
var defaultFilename = 'default.json';
var commonFilename = 'gamesettings.json';

function fakeCallback(err){
	if(err){
		console.error('FCB',err);
		throw err;
	}
}

function change(newSettings, done){
	if(!done){done = fakeCallback;}
	settings = _.assign(settings,newSettings);
	done(null);
}

function save(done){
	if(!done){done = fakeCallback;}
	fs.writeFile(  __dirname + '/'+commonFilename, JSON.stringify( settings ), "utf8", done );
}

function load(done){
	if(!done){done = fakeCallback;}
	readJson(commonFilename,done);
}

function reset(done){
	if(!done){done = fakeCallback;}
	async.series([
		function(cb){
			readJson(defaultFilename,cb)
		},
		function(cb){
			save(cb)
		}
	],done);

}


function readJson(filename,done){
	if(!done){done = fakeCallback;}
	fs.readFile( __dirname + '/'+filename, function (err, data) {

		if (err) {return done(err)}
		try{
			var json = JSON.parse(data);
		}catch(e){
			done(e)
		}
		settings = json;
		done(null,json);
	});
}




module.exports = {
	load:load,
	save:save,
	change:change,
	reset:reset,
	init:function(done){
		if(!done){done = fakeCallback;}
		console.log('db init.')
		async.waterfall([
			function(cb){
				console.log('loading')
				load(function(err){
					cb(null);
				});
			},function(cb){
				console.log('2')
				if(!settings){
					console.log('there is no any saved settings, loading default settings')
					reset(cb);
				}else{
					console.log('Saved data restored.')
					cb(null);
				}
			}
		],done);
	},
	settings:function(){
		return settings;
	}
};