
angular.module('adminapp',[
		'ngSanitize',
		'ui.bootstrap',
		'ui.router',
		'ngFileUpload',
		'pubnub.angular.service',
		'colorpicker.module'
])
	.constant('_', window._)
	.run(function ($rootScope) {
		$rootScope._ = window._;
	})
	.config(function($stateProvider, $urlRouterProvider) {


		$stateProvider
			.state('main', {
				url: '/',
				controller: 'MainAdminCtrl',
				templateUrl: 'pages/admin/main_admin.tmpl.html'
			});

		// if none of the above states are matched, use this as the fallback
		  $urlRouterProvider.otherwise('/');
	});
