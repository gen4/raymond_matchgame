'use strict';

angular.module('gameapp')
	.service('gameService', ['_', '$timeout','$interval', 'pubnubService','$http', function (_,$timeout,$interval,pubnubService, $http) {

		this.unclickable = false;
		this.game = {};
		this.home = true;
		var timerTemp;
		var timerInterval;
		var startTimout;
		var interactiveTimeout;
		//	cols:4,
		//	rows:2,
		//	timer:30,
		//	images:[
		//		{id:0,filename:'sample.png'},
		//		{id:1,filename:'sample.png'},
		//		{id:2,filename:'sample.png'},
		//		{id:3,filename:'sample.png'}
		//	]
		//};

		this.getGame = function(){
			return this.game;
		};


		this.toggleOpen=function(cid){
			var card = _.chain(this.game.cards).flatten().findLast({id:cid}).value();
			if(card.guessed || card.opened){card.unclickable = false; return;}
			card.opened = !card.opened;
			var opened = _.chain(this.game.cards).flatten().filter({opened:true}).value()
			if(opened.length>=2){
				that.game.unclickable = true;
			}
		};


		this.checkWin = function(){
			var unguessded = _.chain(this.game.cards).flatten().filter({guessed:false}).value()
			if(unguessded.length>0){
				console.log('still not win');
			}else{
				console.log('win!');
				this.stopGame(true);
			}
		};

		this.openingComplete = function(){
			var that = this;
			$timeout(function(){
				that.checkPairs()
			},1);
			that.game.unclickable = false;
		};
		this.checkPairs = function(){
			var flatten = _.chain(this.game.cards).flatten();
			this.unclickable = false;
			var opened = flatten.filter({opened:true,guessed:false}).value();
			if(opened && opened.length==this.game.memorize){
				if(_.every(opened,function(item){
						return opened[0].image.filename == item.image.filename;
					})){
					_.each(opened,function(item){
						item.guessed = true;
						item.opened = false;
					});
					this.checkWin();
				}else{
					_.each(opened,function(item){
						item.opened = false;
					})
				}
			}else{

			}
		};

		this.startGame = function(){
			if(timerInterval){$interval.cancel(timerInterval)}
			var that = this;
			this.game.memorizeMode = true;
			timerTemp = this.game.timer;
			this.game.timer = this.game.memorize;
			startTimout = $timeout(function(){
				timerInterval = $interval(that.countDown.bind(that),1000);
			},3000);

		};

		this.startInteractivePart = function(){
			var that = this;
			if(timerInterval){$interval.cancel(timerInterval)}
			this.game.memorizeMode = false;
			this.game.timer = timerTemp;
			interactiveTimeout = $timeout(function() {
				timerInterval = $interval(that.countDown.bind(that), 1000);
			},1000);
		}


		this.countDown = function(){
			this.game.timer--;
			if(this.game.timer<0){
				this.game.timer = 0;
				if(this.game.memorizeMode){
					this.startInteractivePart();
				}else{
					this.stopGame(false);
				}

			}
		};

		this.stopGame = function(win){
			if(timerInterval){$interval.cancel(timerInterval)}
			if(win){
				this.game.isOver = 'win';
			}else{
				this.game.isOver = 'loose';
			}
		};

		this.resetGame = function(cols,rows){
			if(timerInterval){$interval.cancel(timerInterval)}
		}

		function processCards(data){
			var obj = _.clone(data,true);
			if(!obj.images){
				console.error('game is not ready');
				return;}

			var validImages = _.chain(obj.images).filter({available:true}).value();
			var rows,cols;
			if(validImages.length==4){
				cols = 4;
				rows = 2;
			}else if(validImages.length==5){
				cols = 5;
				rows = 2;
			}else if(validImages.length==6){
				cols = 6;
				rows = 2;
			}else if(validImages.length==7){
				cols = 5;
				rows = 3;
			}else if(validImages.length==8){
				cols = 6;
				rows = 3;
			}else if(validImages.length==9){
				cols = 6;
				rows = 3;
			}else{
				console.error('wrong images number')
			}
			//var cols = data.cols;
			//var rows = (cols*2<validImages.length*2)?3:2;
			console.log("! ",validImages.length,rows,cols*2)
			validImages = _.take(validImages,(cols*rows)/2);
			var images = [];

			images = validImages.concat(validImages)

			images = _.shuffle(images);

			console.log('images',images);

			var cards = new Array();

			for(var i=0;i<rows;i++){
				for(var j=0;j<cols;j++){
					if(!cards[i]){
						cards[i] = [];
					}
					var img = images.pop();
					if(!img){
						continue;
					}else{
						cards[i][j]={
							id:(i*cols)+j,
							image:img,
							opened:false,
							guessed:false
						};
					}

				};
			};
			obj.rows=rows;
			obj.cols=cols;
			obj.cards= cards;
			obj.uniq_id = Math.random();
			return obj;
		}

		var that = this;
		function getDefaultSettings(data){
			if(startTimout){$timeout.cancel(startTimout)}
			if(timerInterval){$interval.cancel(timerInterval)}
			if(interactiveTimeout){$timeout.cancel(interactiveTimeout)}
			//$http.get('/defaults').then(function(resp){
				that.game = _.assign(that.game,data);

				//that.game = _.assign(that.game,processCards(resp.data));
				that.game.isOver = null;
				//console.log('def resp',that.game)
				that.startGame();

			//});
		}

		pubnubService.on('startNewGame',function(data){
			getDefaultSettings(data);
		});

		pubnubService.on('goHome',function(data){
			if(timerInterval){$interval.cancel(timerInterval)}
		});

		pubnubService.on('finishGame',function(data){
			if(startTimout){$timeout.cancel(startTimout)}
			if(timerInterval){$interval.cancel(timerInterval)}
			if(interactiveTimeout){$timeout.cancel(interactiveTimeout)}
			that.stopGame(data.isWin)
			//if(timerInterval){$interval.cancel(timerInterval)}
		});

		pubnubService.on('cardClicked',function(data){
			that.toggleOpen(data.id);
		});
	$http.get('http://127.0.0.1:3000/defaults').then(function(resp){
		that.game = _.assign(that.game,resp.data);
	});


}]);
