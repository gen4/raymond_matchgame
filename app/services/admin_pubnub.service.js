angular.module('adminapp')
	.service('pubnubService', ['$http','Pubnub', function ($http,Pubnub) {
		var channel = 'MemoryGame:March:gen4';
		var defaultInstance = Pubnub.init({
			publish_key: "pub-c-0bb265cb-f829-414d-923b-a838e0a9cc04",
			subscribe_key: "sub-c-10c5ffd0-f18f-11e5-8180-0619f8945a4f"
		});

		Pubnub.subscribe({
			channel  : channel,
			message  : onMessage,
			triggerEvents: true
		});

		function onMessage(a,b,c){
			console.log('pub nub ',a,b,c);
		}

		this.send = function(message){
			Pubnub.publish({
				channel  : channel,
				message  : {message:message},
				callback : function(info) { console.log('cb',info) },
				triggerEvents: ['callback']
			});
		};

	}]);
