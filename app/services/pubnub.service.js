'use strict';

angular.module('gameapp')
	.service('pubnubService', ['$http','Pubnub','$rootScope', function ($http,Pubnub,$rootScope) {
		console.log('pub service init');
		var callbacks = {};
		var channel = 'MemoryGame:March:gen4';
		 Pubnub.init({
			publish_key: "pub-c-0bb265cb-f829-414d-923b-a838e0a9cc04",
			subscribe_key: "sub-c-10c5ffd0-f18f-11e5-8180-0619f8945a4f"
		});

		Pubnub.subscribe({
			channel  : channel,
			message  : onMessage
			//triggerEvents: true
		});

		function onMessage(message){
			console.log('pub nub ',message);
			_.each(callbacks[message.message.cmd],function(item){
				item(message.message.data);
			})
		}

		this.on = function(cmd,callback){
			if(!callbacks[cmd]){
				callbacks[cmd] = [];
			}
			callbacks[cmd].push(callback)
		};
		this.send = function(message){
			Pubnub.publish({
				channel  : channel,
				message  : {message:message},
				callback : function(info) { console.log('cb',info) },
				triggerEvents: ['callback']
			});
		};

		this.getDefaultSettings = function(done){
			Pubnub.publish({
				channel  : channel,
				message  : {cmd:'getDefaultSettings'},
				callback : done,
				triggerEvents: ['callback']
			});
		}

	}]);
