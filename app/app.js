
angular.module('gameapp',[
		'ngSanitize',
		'ui.bootstrap',
		'ui.router',
		'pubnub.angular.service'
])
	.constant('_', window._)
	.run(function ($rootScope) {
		$rootScope._ = window._;
	})
	.config(function($stateProvider, $urlRouterProvider) {

		//treeConfig.defaultCollapsed = true; // collapse nodes by default

		$stateProvider
			.state('main', {
				url: '/',
				controller: 'MainCtrl',
				templateUrl: 'pages/main.tmpl.html'
			})

		// if none of the above states are matched, use this as the fallback
		  $urlRouterProvider.otherwise('/');
	});

