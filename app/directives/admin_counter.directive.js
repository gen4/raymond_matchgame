angular.module('adminapp').directive('counter',function() {
	return {
		restrict: 'E',
		scope: {mainclass:'@',setting:'=', field:'@', min:"=", max:"=", save:'&'},
		replace: true,
		templateUrl: 'directives/admin_counter.tmpl.html',
		link: function (scope, element) {
			//console.log('A ',scope.setting,scope.field,scope.setting[scope.field])
			//scope.ngModel = ;
			//scope.$watch(function(){
			//	return scope.setting[scope.field];
			//},function(n){
			//	scope.setting[scope.field]
			//});
			scope.add = function(){
				if(!_.isNumber(scope.setting[scope.field])){scope.setting[scope.field] = 1;}
				if(scope.setting[scope.field]>=scope.max){
					scope.setting[scope.field] = scope.max;
					return;
				}
				scope.setting[scope.field]++;
				scope.save()
			}
			scope.sub = function(){
				if(scope.setting[scope.field]<=scope.min){
					scope.setting[scope.field] = scope.min;
					return;
				}
				scope.setting[scope.field]--;
				scope.save();
			}
		}
	};
});
