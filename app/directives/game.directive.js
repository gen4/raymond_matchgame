angular.module('gameapp').directive('game',['gameService',function(gameService) {
	return {
		restrict: 'E',
		scope: {onoff:'=',onDone:'&'},
		replace: true,
		templateUrl: 'directives/game.tmpl.html',
		link: function (scope, element) {


			scope.game=gameService.getGame();

			scope.$watch(function(){
				return scope.onoff;
			},function(n,o){
				console.log('game onoff',n)
				//if(n==o){return}
				if(scope.onoff==true){
					showingUp();
				}else{
					//hide();
				}
			});
			scope.$watch(function(){
				return scope.game.uniq_id;
			},function(n,o){
				console.log('game onoff',n)
				//if(n==o){return}
				if(scope.onoff==true){
					showingUp();
				}else{
					//hide();
				}
			});
			var game = $('#game');
			var gameLogo = $('#game-logo');
			var leftCard = $('#left-card');
			var rightCard = $('#right-card');
			var sponsorLogo = $('#sponsor-logo');
			var cardClir = 200;
			var easing = 'easeOutQuart';
			var duration = 1200;

			function showingUp(){
				var logopos = (scope.game.rows==2)?-120:-180;
				console.log('GM ',scope.game.cols,scope.game.rows);
				if(scope.game.cols==6){
					scope.girdOffset = (scope.game.rows==3)?'-30px':'60px';
				}else{
					scope.girdOffset = (scope.game.rows==3)?'-70px':'0px';
				}
				console.log('game turn on');
				gameLogo.velocity({
					properties: {opacity: [1,0],top:[logopos,-260], scale:[1,1.1]},
					options: { duration: duration, delay:0, easing: easing}
				});

				leftCard.velocity({
					properties: {opacity: [0,0],left:[-260-cardClir,-260-cardClir]},
					options: { duration: 1, delay:0, easing: easing}
				});
				leftCard.velocity({
					properties: {opacity: [1,0],left:[cardClir,-260-cardClir]},
					options: { duration: duration, delay:200, easing: easing}
				});

				rightCard.velocity({
					properties: {opacity: [0,0],right:[-260+cardClir,-260+cardClir]},
					options: { duration: 1, delay:0, easing: easing}
				});
				rightCard.velocity({
					properties: {opacity: [1,0],right:[cardClir,-260+cardClir]},
					options: { duration: duration, delay:600, easing: easing}
				});

				//if(scope.game.sponsor.filename){
				sponsorLogo.velocity({
					properties: {opacity: [0,0]},
					options: { duration: 1, delay:0, easing: easing}
				});
					sponsorLogo.velocity({
						properties: {opacity: [1,0],top:[logopos+200,-60], scale:[1,1.3]},
						options: { duration: duration, delay:400, easing: easing}
					});
				//}

			}

			function turnOff(){
				console.log(' game turn off');
				game.velocity({
					properties: {opacity: [0,1],scale:[1.2,1]},
					options: { duration: 1500, delay:2000, complete:scope.onDone }
				});
			}

		}
	};
}]);
