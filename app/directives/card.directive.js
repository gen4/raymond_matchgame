angular.module('gameapp').directive('card',['gameService','$timeout','pubnubService',function(gameService,$timeout,pubnubService) {
	return {
		restrict: 'E',
		scope: {card:'='},
		replace: true,
		templateUrl: 'directives/card.tmpl.html',
		link: function (scope, element) {
			var easingOut = 'easeOutQuart';
			var easingIn = 'easeInQuart';
			var flipTimer;
			var unclickable = false;
			var delay = Math.random()*500+1000;
			scope.frontSide=false;
			scope.zoom = getZoomByCols(gameService.game)
			scope.toggleOpen = function(){
				if(unclickable || scope.card.guessed || gameService.game.unclickable || gameService.game.memorizeMode){return;}
				unclickable = true;
				pubnubService.send({cmd:'cardClicked',data:{id:scope.card.id}});
				gameService.toggleOpen(scope.card.id);
			};



			//
			scope.$watch(function(){
				return  gameService.game.memorizeMode || scope.card.opened ;
			},function(n,o){
				console.log('crd',n,o);
				//if(n==o){return;}
				if(n==true){
					turnOn();
				}else{
					delay=0;
					turnOff()
				}
			});
			//scope.$watch(function(){
			//	return gameService.game.memorizeMode;
			//},function(n){
			//	if(!n){
			//
			//	}
			//})
			//turnOn();
			function turnOn(){
				var cardDom = $('#card_'+scope.card.id);
				function firstPart(){
					cardDom.velocity("stop");
					if(flipTimer){$timeout.cancel(flipTimer)}
					cardDom.velocity({
						properties: {scale:[1.2,1]},
						options: { duration: 700, delay:delay,complete:secondPart, easing: easingOut }
					});
					cardDom.velocity({
						properties: {rotateY:['180deg','0deg']},
						options: { duration: 1000, delay:delay, easing: easingOut, queue: false  }
					});
					flipTimer = $timeout(function(){
						scope.frontSide=true;
					},140+delay)
				}


				function secondPart(){
					cardDom.velocity({
						properties: {scale:[1,1.2]},
						options: { duration: 700, delay:0 ,easing: easingIn, complete:openingComplete}
					});
				};
				firstPart()
			}

			function openingComplete(){
				gameService.openingComplete()
			}

			function getZoomByCols(game){
				var cols = game.cols;
				var rows = game.rows;
				//var subzoom = (scope.game.rows==2)?1:0.8;
				if(cols==6 ){
					return 0.6;
				}else if(cols==5 || rows==3){
					return 0.8;
				}else if(cols==4){
					return 1;
				}
			}
			//
			function turnOff(){

				if(scope.card.guessed){return;}
				var cardDom = $('#card_'+scope.card.id);
				var mult= 0.5;
				function firstPart(){
					cardDom.velocity("stop");
					if(flipTimer){$timeout.cancel(flipTimer)}
					cardDom.velocity({
						properties: {scale:[1.2,1]},
						options: { duration: 700*mult, delay:0,complete:secondPart, easing: easingOut }
					});
					cardDom.velocity({
						properties: {rotateY:['0deg','180deg']},
						options: { duration: 1000*mult, delay:0, easing: easingOut, queue: false  }
					});
					flipTimer = $timeout(function(){
						scope.frontSide=false;
					},140*0.5)
				}


				function secondPart(){
					cardDom.velocity({
						properties: {scale:[1,1.2]},
						options: { duration: 700*mult, delay:0 ,easing: easingIn, complete:offComplete}
					});
				};
				firstPart()
			}
			function offComplete(){
				unclickable = false;
			}
		}
	};
}]);
