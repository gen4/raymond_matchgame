angular.module('adminapp').directive('slider',['pubnubService','$http',function(pubnubService,$http) {
	return {
		restrict: 'E',
		scope: {mainclass:'@',ngModel:'=', upload:'=', setting:'='},
		replace: true,
		templateUrl: 'directives/slider.tmpl.html',
		link: function (scope, element) {
			scope.offset = 0;
			var position = 0;
			var shiftOffset = 120;

			scope.swipeLeft = function(){

				position++;
				if(position>=(scope.ngModel.length-5)){
					position=(scope.ngModel.length-5);
					if(position<=0){position=0;}
				}
				scope.offset=-shiftOffset*position;
			};

			scope.swipeRight = function(){

				position--;
				if(position<=0){
					position=0;
				}
				scope.offset=-shiftOffset*position;
			}

			scope.chageAvailable = function(image){
				image.available = !image.available;
				pubnubService.send({cmd:'changeAvailable',data:{isAvailable:image.available, filename:image.filename}});
			};

			scope.deleteImage = function(image){
				console.log(image);
				scope.setting.images = _.reject(scope.setting.images,function(item){
					return item.filename == image.filename
				});
				pubnubService.send({cmd:'newSettings',data:scope.setting});
				$http.delete('/file/'+image.filename).then(function(){
					console.log('deleted');
				})
			}
		}
	};
}]);
