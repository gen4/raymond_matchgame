angular.module('adminapp').directive('colorPicker',function() {
	return {
		restrict: 'E',
		scope: {setting:'=', save:'&'},
		replace: true,
		templateUrl: 'directives/color-picker.tmpl.html',
		link: function (scope, element) {
			scope.$on('colorpicker-selected',function(e,data){
				if(data.value){
					scope.setting.fontcolor = data.value;
					scope.save();
				}
			});
		}
	};
});
