angular.module('gameapp').directive('finalScreen',['gameService', function(gameService) {
	return {
		restrict: 'E',
		scope: {game:'='},
		replace: true,
		templateUrl: 'directives/final-screen.tmpl.html',
		link: function (scope, element) {
			var game = gameService.game;
			scope.game = gameService.game;
			scope.$watch(function(){
				return game.isOver;
			},function(n,o){
				console.log('game over changed',n);
				if(n){
					turnOn(n);
				}else{
					turnOff();
				}
			});
			var obj = $('#fs-text');
			function turnOn(){
				console.log('final turn on');
				obj.velocity({
					properties: {opacity: [1,0],scale:[1.2,0]},
					options: { duration: 1500, delay:0, easing:'spring' }
				});
			}

			function turnOff(){
				console.log('final turn off');
				//obj.velocity({
				//	properties: {opacity: [0,1],scale:[1.9,1]},
				//	options: { duration: 1500, delay:2000, complete:scope.onDone }
				//});
			}
		}
	};
}]);
