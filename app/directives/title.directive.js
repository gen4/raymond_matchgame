angular.module('gameapp').directive('title',['gameService',function(gameService) {
	return {
		restrict: 'E',
		scope: {onoff:'=',onDone:'&'},
		replace: true,
		templateUrl: 'directives/title.tmpl.html',
		link: function (scope, element) {
			scope.game=gameService.getGame();


			scope.$watch('onoff',function(n,o){
				if(n==true){
					turnOn();
				}else{
					turnOff();
				}
			});
			var title = $('#title');
			function turnOn(){
				console.log('turn on');
				title.velocity({
					properties: {opacity: [1,0],scale:[1,1.2]},
					options: { duration: 1500, delay:0 }
				});
			}

			function turnOff(){
				console.log('turn off');
				title.velocity({
					properties: {opacity: [0,1],scale:[1.2,1]},
					options: { duration: 1500, delay:0, complete:scope.onDone }
				});
			}
		}
	};
}]);
