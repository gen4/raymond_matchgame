angular.module('gameapp').controller('MainCtrl', ['$scope', '$state', 'pubnubService', '$timeout','gameService',function($scope, $state, pubnubService, $timeout,gameService) {
	$scope.titleIsOn = false;
	$scope.gameIsOn = false;
	$scope.game=gameService.game;
	$scope.showTitle = function(){
		$timeout(function(){
			$scope.titleIsOn = true;
			$scope.gameIsOn = false;
		},1);


	};
	$scope.showGameField = function(){
		$timeout(function() {
			$scope.gameIsOn = true;
			$scope.titleIsOn = false;
		},1);
	};

	pubnubService.on('goHome',function(data){
		$scope.showTitle();
	});

	pubnubService.on('startNewGame',function(data){
		$timeout(function(){
			$scope.showGameField();
		},100);
	});

	$scope.showTitle();

}]);
