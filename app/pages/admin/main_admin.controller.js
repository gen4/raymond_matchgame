angular.module('adminapp').controller('MainAdminCtrl', ['$scope', '$state', 'Upload','pubnubService','$http',function($scope, $state,Upload,pubnubService,$http) {
	console.log('rout');



	$scope.upload = function (file) {
		Upload.upload({
			url: 'upload',
			data: {file: file, 'username':'any', name:'asd'}
		}).then(function (resp) {
			//console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
			getDefaultSettings();
		}, function (resp) {
			console.log('Error status: ' + resp.status);
		}, function (evt) {
			//var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
			//console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
		});
	};

	$scope.loadSponsor = function(file){
		Upload.upload({
			url: 'sponsor',
			data: {file: file, 'username':'any', name:'asd'}
		}).then(function (resp) {
			//console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
			getDefaultSettings();
		}, function (resp) {
			console.log('Error status: ' + resp.status);
		}, function (evt) {
			//var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
			//console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
		});
	};

	$scope.deleteSponser = function(){
		$http.delete('/sponsor').then(function(resp){
			console.log('deleted sponser',resp);
			getDefaultSettings();
		});
	};

	$scope.setting = {
		images:[]
	};

	$scope.setCols = function(howmany){
		$scope.setting.cols = howmany;
		$scope.saveSettings();
	}

	$scope.reloadGame = function(){
		pubnubService.send({cmd:'startNewGame',data:processCards($scope.setting)});
	};

	$scope.goHome = function(){
		pubnubService.send({cmd:'goHome'});
	};

	$scope.saveSettings = function(){
		pubnubService.send({cmd:'newSettings',data:$scope.setting})
	};


	$scope.finishGame = function(isWin){
		pubnubService.send({cmd:'finishGame',data:{isWin:isWin}});
	}

	function getDefaultSettings(){
		$http.get('/defaults').then(function(resp){
			console.log('def resp',resp)
			$scope.setting = resp.data;
		});
	}
	getDefaultSettings();

	function processCards(data){
		var obj = _.clone(data,true);
		if(!obj.images){
			console.error('game is not ready');
			return;}

		var validImages = _.chain(obj.images).filter({available:true}).value();
		var rows,cols;
		if(validImages.length==4){
			cols = 4;
			rows = 2;
		}else if(validImages.length==5){
			cols = 5;
			rows = 2;
		}else if(validImages.length==6){
			cols = 6;
			rows = 2;
		}else if(validImages.length==7){
			cols = 5;
			rows = 3;
		}else if(validImages.length==8){
			cols = 6;
			rows = 3;
		}else if(validImages.length==9){
			cols = 6;
			rows = 3;
		}else{
			console.error('wrong images number')
		}
		//var cols = data.cols;
		//var rows = (cols*2<validImages.length*2)?3:2;
		console.log("! ",validImages.length,rows,cols*2)
		validImages = _.take(validImages,(cols*rows)/2);
		var images = [];

		images = validImages.concat(validImages)

		images = _.shuffle(images);

		console.log('images',images);

		var cards = new Array();

		for(var i=0;i<rows;i++){
			for(var j=0;j<cols;j++){
				if(!cards[i]){
					cards[i] = [];
				}
				var img = images.pop();
				if(!img){
					continue;
				}else{
					cards[i][j]={
						id:(i*cols)+j,
						image:img,
						opened:false,
						guessed:false
					};
				}

			};
		};
		obj.rows=rows;
		obj.cols=cols;
		obj.cards= cards;
		obj.uniq_id = Math.random();
		return obj;
	}

}]);